# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject =
  Subject.new(
    'Menteuse ?',
    'Alice dit que les dés ne sont pas pipés'
  )

puts subject

subject.hypotheses <<
  Hypothesis.new('Alice dit la vérité',
                 prior_probability: Rational('999/1000'))
subject.hypotheses <<
  Hypothesis.new('Alice ment',
                 prior_probability: Rational('1/1000'))

subject.evidence = Evidence.new('Le premier dé donne 6')
subject.set_evidence_likelihoods(Rational('1'), Rational('1'))

puts subject.format_current_state(:cycle)

2.upto(10).each do |number|
  subject.evidence = Evidence.new("Le dé donne 6 pour la #{number}ème fois de suite.")
  subject.set_evidence_likelihoods(Rational('1/6'), Rational('5/6'))

  puts "NEW EVIDENCE: #{subject.evidence.description} (likelihoods: #{subject.evidence_likelihoods})"
  puts "Posterior Probabilities for each hypothesis: #{subject.posterior_probabilities(:cycle)}"
end
