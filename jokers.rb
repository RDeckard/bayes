# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject =
  Subject.new(
    'Jokers problem',
    'Alice has two cards in her hand'
  )

subject.hypotheses <<
  Hypothesis.new('Alice has two jokers',
                 prior_probability: Rational('2/54') * Rational('1/53'))
subject.hypotheses <<
  Hypothesis.new('Alice has no jokers',
                 prior_probability: Rational('50/54') * Rational('49/53'))
subject.hypotheses <<
  Hypothesis.new('Alice has one and only one joker',
                 prior_probability: 1 - subject.hypotheses.sum(&:prior_probability))

puts subject

subject.evidence = Evidence.new('Alice says that she has at least one joker')
subject.set_evidence_likelihoods(Rational('1'), Rational('0'), Rational('1'))

puts subject.format_current_state(:cycle)

subject.evidence = Evidence.new("Alice answered 'yes' to the question 'Have you the red joker'")
subject.set_evidence_likelihoods(Rational('1'), Rational('0'), Rational('1/2'))

puts
puts subject.format_current_state
