# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject = Subject.new('7 billion places to the heaven, 1000 to the hell')

subject.hypotheses << Hypothesis.new('going to heaven',
                                     prior_probability: Rational('900/1_000'))

subject.hypotheses << Hypothesis.new('going to hell',
                                     prior_probability: Rational('100/1_000'))

subject.evidence = Evidence.new('You wake up.')
subject.set_evidence_likelihoods(Rational('1/2'), Rational('1/2'))

puts subject.format_current_state
