# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject = Subject.new('Disease')

subject.hypotheses << Hypothesis.new('healthy',
                                     prior_probability: Rational('9999/10000'))
subject.hypotheses << Hypothesis.new('sick',
                                     prior_probability: Rational('1/10000'))

[
  [
    Evidence.new('Positive test'),
    [Rational('1/100'), Rational('1')]
  ],
  [
    Evidence.new('second positive test'),
    [Rational('1/100'), Rational('1')]
  ],
  [
    Evidence.new('third positive test'),
    [Rational('1/100'), Rational('1')]
  ]
].each do |evidence, evidence_likelihoods|
  subject.evidence = evidence
  subject.set_evidence_likelihoods(*evidence_likelihoods)

  puts subject.format_current_state(:cycle)
  puts
end
