# frozen_string_literal: true

module Bayes
  class Hypothesis
    attr_accessor :description, :prior_probability

    def initialize(description, prior_probability: Rational('0'))
      @description = description
      @prior_probability = prior_probability
    end

    def posterior_probability(evidence, evidence_likelihoods)
      evidence_likelihoods[self] * @prior_probability / evidence.marginal_likelihood(evidence_likelihoods)
    end
  end
end
