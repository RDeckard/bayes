# frozen_string_literal: true

module Bayes
  class Evidence
    attr_accessor :description

    def initialize(description)
      @description = description
    end

    def marginal_likelihood(likelihoods)
      likelihoods.map do |hypothesis, likelihood|
        likelihood * hypothesis.prior_probability
      end.sum
    end
  end
end
