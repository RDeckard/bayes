# frozen_string_literal: true

module Bayes
  class Subject
    attr_accessor :name, :description, :hypotheses, :evidence

    PERCENT_FORMATER = ->(numeric) { "#{(numeric.to_f * 100).round(2)}%" }

    def initialize(name, description = nil)
      @name = name
      @description = description
      @hypotheses = []
      @evidence_likelihoods = {}
    end

    def prior_probabilities
      hypotheses.to_h do |hypothesis|
        [hypothesis.description, hypothesis.prior_probability]
      end.transform_values(&PERCENT_FORMATER)
    end

    def evidence_likelihoods
      @hypotheses.to_h do |hypothesis|
        [hypothesis.description, @evidence_likelihoods[hypothesis] || nil]
      end
    end

    def set_evidence_likelihoods(*likelihoods)
      return "You have to give #{@hypotheses.count} likelihoods" unless likelihoods.count == @hypotheses.count

      @hypotheses.each_with_index do |hypothesis, i|
        set_evidence_likelihood(hypothesis, likelihoods[i])
      end
    end

    def set_evidence_likelihood(hypothesis, likelihood)
      return 'unknowned hypothesis' unless @hypotheses.include?(hypothesis)

      @evidence_likelihoods.merge!(
        hypothesis => likelihood
      )
    end

    def posterior_probabilities(option = nil)
      post_probabilities =
        @hypotheses.to_h do |hypothesis|
          [hypothesis, hypothesis.posterior_probability(@evidence, @evidence_likelihoods)]
        end

      if option == :cycle
        @hypotheses.each do |hypothesis|
          hypothesis.prior_probability = post_probabilities[hypothesis]
        end
      end

      post_probabilities.transform_keys!(&:description).transform_values(&PERCENT_FORMATER)
    end

    def check
      case
      when string_not_empty?(@name)
        'Subject must have a name'
      when @hypotheses.select { |h| h.is_a? Hypothesis }.empty?
        'Subject must have at least one hypothesis'
      when @hypotheses.select { |h| string_not_empty?(h.description) }.any?
        'One or more hypotheses have no description'
      when @hypotheses.select { |h| h.prior_probability.to_f.zero? }.any?
        'One or more hypotheses have no prior probability'
      when @hypotheses.select { |h| h.prior_probability.to_f > 1 }.any?
        "A prior probability can't be greater than one"
      when @hypotheses.sum(&:prior_probability) != 1
        'The sum of hypotheses prior probabilities must be 1'
      when !@evidence.is_a?(Evidence)
        'Ok. You should give a new evidence now'
      when string_not_empty?(@evidence.description)
        'The given evidence must have a name'
      when !@evidence_likelihoods.is_a?(Hash) || @hypotheses.reject { |h| @evidence_likelihoods[h].is_a? Rational }.any?
        'One or more likelihoods is missing for some hypotheses'
      else
        'Saul Goodman'
      end
    end

    def format_current_state(option = nil)
      <<~STATE
        NEW EVIDENCE: #{evidence.description}
        Evidence Likelihoods by hypothesis:
          #{evidence_likelihoods}
        Prior Probabilities for each hypothesis:
          #{prior_probabilities}
        Posterior Probabilities for each hypothesis:
          #{posterior_probabilities(option)}
      STATE
    end

    def to_s
      %(SUBJECT "#{name}": #{description})
    end

    private

    def string_not_empty?(object)
      !object.is_a?(String) || object.strip.empty?
    end
  end
end
