# frozen_string_literal: true

require_relative 'subject'
require_relative 'hypothesis'
require_relative 'evidence'
