# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject = Subject.new('Goats and Car')

subject.hypotheses << Hypothesis.new('Car in 1',
                                     prior_probability: Rational('1/3'))
subject.hypotheses << Hypothesis.new('Car in 2',
                                     prior_probability: Rational('1/3'))
subject.hypotheses << Hypothesis.new('Car in 3',
                                     prior_probability: Rational('1/3'))

subject.evidence = Evidence.new('Choose 1')
subject.set_evidence_likelihoods(Rational('1/3'), Rational('1/3'), Rational('1/3'))

puts subject.format_current_state(:cycle)

subject.evidence = Evidence.new('opening 2 to showing a goat')
subject.set_evidence_likelihoods(Rational('1/2'), Rational('0'), Rational('1'))

puts
puts subject.format_current_state
