# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject = Subject.new('2 children (sorted by descending age)')

subject.hypotheses << Hypothesis.new('girl & girl',
                                     prior_probability: Rational('1/4'))
subject.hypotheses << Hypothesis.new('girl & boy',
                                     prior_probability: Rational('1/4'))
subject.hypotheses << Hypothesis.new('boy & girl',
                                     prior_probability: Rational('1/4'))
subject.hypotheses << Hypothesis.new('boy & boy',
                                     prior_probability: Rational('1/4'))

puts subject

subject.evidence = Evidence.new('The first child is a boy.')
subject.set_evidence_likelihoods(Rational('0'), Rational('0'), Rational('1'), Rational('1'))

puts subject.format_current_state
puts

subject.evidence = Evidence.new('The first of those children we hear about is a boy.')
subject.set_evidence_likelihoods(Rational('0'), Rational('1/2'), Rational('1/2'), Rational('1'))

puts subject.format_current_state
puts

subject.evidence = Evidence.new('Have you at least one boy? - Yes.')
subject.set_evidence_likelihoods(Rational('0'), Rational('1'), Rational('1'), Rational('1'))

puts subject.format_current_state
