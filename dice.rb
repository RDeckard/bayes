# frozen_string_literal: true

require_relative 'lib/bayes'

module Bayes
  module Dices
    extend self

    def run(dices_by_nb_of_faces = [4, 6, 8, 10, 12, 20], trials_nb: 5)
      subject =
        Subject.new('Which dice?')

      dices_by_nb_of_faces.each do |nb_of_faces|
        subject.hypotheses <<
          Hypothesis.new(nb_of_faces.to_s,
                         prior_probability: Rational("1/#{dices_by_nb_of_faces.count}"))
      end

      puts subject

      secret_dice = dices_by_nb_of_faces.sample
      1.upto(trials_nb) do |turn|
        puts "TURN ##{turn}:"
        number = rand(1..secret_dice)
        subject.evidence = Evidence.new("We get a #{number}")
        subject.hypotheses.each do |hypothesis|
          subject.set_evidence_likelihood(
            hypothesis,
            if hypothesis.description.to_i >= number
              Rational("1/#{hypothesis.description}")
            else
              Rational('0')
            end
          )
        end
        puts subject.format_current_state(:cycle)
        puts
      end

      puts "The secret dice was the #{secret_dice}"
    end
  end
end

Bayes::Dices.run
