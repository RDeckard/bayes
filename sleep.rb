# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject = Subject.new('The red pill will you make you sleep 2 times without remember the first time at the second time. The blue pill will you make you sleep only one time')

subject.hypotheses << Hypothesis.new('red',
                                     prior_probability: Rational('1/2'))
subject.hypotheses << Hypothesis.new('blue',
                                     prior_probability: Rational('1/2'))

puts subject

subject.evidence = Evidence.new('We wake you up.')
subject.set_evidence_likelihoods(Rational('1/2'), Rational('1/2'))

puts subject.format_current_state
