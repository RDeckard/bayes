# frozen_string_literal: true

require_relative 'lib/bayes'

include Bayes

subject = Subject.new('Who is M.')

subject.hypotheses << Hypothesis.new('a G.',
                                     prior_probability: Rational('1/5'))
subject.hypotheses << Hypothesis.new('a P.',
                                     prior_probability: Rational('4/5'))

[
  [
    Evidence.new('fully available on presence'),
    [Rational('9/10'), Rational('1/2')]
  ],
  [
    Evidence.new('fixed schedules'),
    [Rational('9/10'), Rational('1/2')]
  ]
].each do |evidence, evidence_likelihoods|
  subject.evidence = evidence
  subject.set_evidence_likelihoods(*evidence_likelihoods)

  puts subject.format_current_state(:cycle)
  puts
end
